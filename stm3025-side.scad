side_dim = [17, 125, 30+5];
peg_distance = 102.5;
peg_d = 5;
peg_h = 3;
angle = 30;
flat = 0.1;


module top(pegs=true) {
    rotate([angle, 0, 0]) {
        cube([side_dim.x, side_dim.y, flat]);


        if ( pegs ) {
            peg_dy = (side_dim.y - peg_distance) / 2;
            for (y = [peg_dy, peg_dy + peg_distance]) {
                translate([side_dim.x/2, y, 0]) {
                    cylinder(d=peg_d, h=peg_h+flat, $fn=16);
                }
            }
        }
    }
}

intersection() {
    union() {
        translate([0, 0, side_dim.z]) { top(); }
        hull() {
            translate([0, 0, side_dim.z]) top(pegs=false);
            linear_extrude(flat) projection(cut=false) top();
        }
    }
    linear_extrude(side_dim.z+side_dim.y+side_dim.x)
        offset(r=(side_dim.x/2 - flat), $fn=32)
        offset(r=-(side_dim.x/2 - flat))
        projection(cut=false)
        top();
}


