/* https://www.badgerandblade.com/forum/wiki/Double-Edge_DE_Razor_Blade_Dimensions_Table */
peg_d = 4.8;                    /* hole diameter >=4.95 */
peg_distance = 25.5;            /* self measured */
blade_dim = [43, 23];

bottom_h = 5;
peg_h = 5;
thickness = 1.8;
slot_thickness = 0.8;

bin_inner_dim = [blade_dim.x + 2, blade_dim.y + 2, 6];
bin_outer_dim = bin_inner_dim + [2*thickness, 2*thickness, thickness];

bottom_r = (bin_outer_dim.y^2 + 4*bottom_h^2) / (8*bottom_h);

echo(bottom_r);

/* for (x = [-peg_distance/2, 0, peg_distance/2]) { */
/*     translate([x, 0, bin_outer_dim.z]) { */
/*         h = bottom_h+thickness+peg_h-peg_d/2; */
/*         cylinder(d=peg_d, h=h, $fn=16); */
/*         translate([0, 0, h]) { */
/*             sphere(d=peg_d, $fn=16); */
/*         } */
/*     } */
/* } */


module body (delta=0) {
    dim = bin_outer_dim - [delta*2, delta*2, delta];
    r = bottom_r - delta;
    top_h = bottom_h - delta;
    intersection() {
        h = top_h + dim.z + thickness;
        translate([0, 0, -r+h+delta]) {
            rotate([0, 90, 0]) {
                cylinder(r=r, h=dim.x, center=true);
            }
        }

        translate([-dim.x/2, -dim.y/2, delta]) {
            cube([dim.x, dim.y, h+1]);
        }
    }
}

difference() {
    body();
    body(thickness);

    slot_r=bottom_r-thickness;
    intersection() {
        translate([bin_outer_dim.x/2, 0, -slot_r+bin_outer_dim.z+bottom_h]) {
            rotate([0, 90, 0]) {
                difference() {
                    h=thickness+2;
                    cylinder(r=slot_r, h=h, center=true);
                    cylinder(r=slot_r-slot_thickness, h=h, center=true);
                }
            }
        }
        cube([bin_outer_dim.x+2, bin_inner_dim.y, (bin_outer_dim.z+bottom_h+5)*2], center=true);
    }
}

