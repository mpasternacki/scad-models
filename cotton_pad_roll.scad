roll_diameter = 160;

thickness = 1.4;
margin = 0.6;

roller_h = 18;
roller_hub_d = 20;
roller_hub_dh = 5;

n_wheels = 6;

module roller_top($fn=96) {
    difference() {
        cylinder(d=roll_diameter, h=roller_h);
        translate([0, 0, -0.01]) {
            difference() {
                cylinder(d=roll_diameter - 2*thickness, h=roller_h);
                translate([0, 0, roller_hub_dh+0.01]) {
                    cylinder(d=roller_hub_d - margin, h=roller_h);
                }
            }
        }
    }
}

module roller_bottom($fn=96) {
    difference() {
        cylinder(d = roll_diameter - 4*thickness, h=roller_h);
        translate([0, 0, -0.01]) {
            cylinder(d=roller_hub_d + margin, h=roller_h+0.02);
        }
    }
}

// color("blue", alpha=0.01) translate([0, 0, thickness]) roller_top();
roller_bottom();
