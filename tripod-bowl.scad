use <exponential-column.scad>;

preview_context = false;

centers=[
    [0, 67.5+2.5],
    [-58.5 - 2.5*sqrt(3)/2, -33.8-2.5/2],
    [58.5 + 2.5*sqrt(3)/3, -33.8-2.5/2],
    ];

r0 = centers[0].y;
r_bottom = r0-32;


if (preview_context && $preview) {
    color("green", alpha=0.1) translate([0,0,50]) {
        intersection() {
            import("vendor/Tripod_base.stl", convexity=5);
            cylinder(r=80, h=5);
        }
    }
    color("pink", alpha=0.1) for(pos=centers) {
        translate(pos) rotate([0,0,sign(pos.x)*240]){
            import("tripod-foot.stl", convexity=5);
        }
    };
}

module bowl () {
    difference() {
        maybe_exponential_column(r1=r_bottom+12, r2=r0, h=32, $layer=1, $fn=128);
        for(pos=centers) {
            translate([pos.x, pos.y, -0.01]) {
                maybe_exponential_column(r1=34.3, r2=14.3, h=53.01, $layer=1);
            }
        }
    }
}

difference () {
    bowl();
    //  difference() {
        translate([0, 0, 2.5]) resize([r0*2-4, r0*2-3, 0]) bowl();
        //cylinder(r=r0, h=2);
//    }
}
