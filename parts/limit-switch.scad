module limit_switch() {
    translate([-7, 0, 0]) {
        difference() {
            union() {
                cube([10, 19, 6]);
                translate([-1.5, 6, 1]) {
                    cube([2, 1.5, 4]);
                }
                rotate([0, 0, 5]) {
                    translate([-1.1, 3.5, 1]) {
                        cube([0.2, 16, 4]);
                    }
                }

                for (y=[2,10,17]) {
                    translate([9, y, 1]) {
                        cube([1.5, 0.5, 4]);
                        translate([1, 0, 1.5]) {
                            cube([3.5, 0.5, 1]);
                        }
                    }
                }
            }

            translate([-1, 12, -1]) {
                cube([2, 10, 8]);
            }

            for(y=[5, 14]) {
                translate([7, y, -1]) {
                    cylinder(d=2.5, h=8, $fn=12);
                }
            }
        }
    }
}
