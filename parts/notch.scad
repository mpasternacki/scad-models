module notch_2d(dim, corner_r, margin=0) {
    assert(corner_r <= dim.y/2);

    difference() {
        square(dim, center=true);
        translate([dim.x/2-corner_r, -dim.y/2]) {
            difference() {
                square(corner_r);
                translate([0, corner_r]) {
                    circle(r=corner_r);
                }
            }
        }
        translate([-dim.x/2, -dim.y/2]) {
            difference() {
                square(corner_r);
                translate([corner_r, corner_r]) {
                    circle(r=corner_r);
                }
            }
        }
    }

    translate([dim.x/2, dim.y/2-corner_r]) {
        difference() {
            square(corner_r);
            translate([corner_r, 0]) {
                circle(r=corner_r);
            }
        }
    }

    translate([-dim.x/2-corner_r, dim.y/2-corner_r]) {
        difference() {
            square(corner_r);
            circle(r=corner_r);
        }
    }

    if (margin>0) {
        translate([0, (dim.y+margin)/2]) {
            square([dim.x+2*corner_r, margin], center=true);
        }
    }
}

module notch_xz(dim, corner_r, margin=0) {
    translate([0, dim.y/2, dim.z/2]) {
        rotate([90, 0, 0]) {
            linear_extrude(dim.y) {
                notch_2d([dim.x, dim.z], corner_r=corner_r, margin=margin);
            }
        }
    }
}

module notch_yz(dim, corner_r, margin=0) {
    rotate([0, 0, 90]) {
        notch_xz([dim.y, dim.x, dim.z], corner_r=corner_r, margin=margin);
    }
}
