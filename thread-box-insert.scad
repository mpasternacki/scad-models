use <dotSCAD/src/rounded_square.scad>;

box_dim = [220, 155, 55];
bobbin_d = 21;
bobbin_h = 13;
spool_d = 24;                   // drima
spool_h = 43;                   // drima
spool_d = 20;                   // gütermann
spool_h = 56;                   // gütermann
spool_d = 20.5;                 // ariadna
spool_h = 60;                   // ariadna
spool_d = 27;                   // mettler
spool_h = 57.5;                 // mettler
spool_d = 30;                   // belfil
spool_h = 75.5;                 // belfil


round_r = 4;
sep = 1;
side_thickness = 2;

max_d = max(bobbin_d, spool_d);
top_h = spool_d/2 + side_thickness; // to maximize height: box_dim.z - (spool_d/2);
inner_dim = [box_dim.x - 2 * round_r, box_dim.y - 2 * round_r];

grid = [spool_h + bobbin_h + 2*sep, max_d + sep];

count = [ floor((inner_dim.x + sep) / grid.x),
          floor((inner_dim.y + sep) / grid.y) ];
echo(count);
center = [ count.x * grid.x - sep, count.y * grid.y - sep ] / 2;

difference() {
    linear_extrude(top_h) {
        rounded_square([box_dim.x, box_dim.y], corner_r=round_r, center=true);
    }

    translate([0, 0, top_h]) {
        for ( x = [0:count.x-1], y=[0:count.y-1]) {
            translate([x*grid.x, y*grid.y] - center) {
                translate([bobbin_h/2, max_d/2]) {
                    rotate([0, 90, 0]){
                        cylinder(h=bobbin_h, d=bobbin_d, center=true);
                    }
                }
                translate([bobbin_h + sep + spool_h/2, max_d/2]) {
                    rotate([0, 90, 0]){
                        cylinder(h=spool_h, d=spool_d, center=true);
                    }
                }
            }
        }
    }

    side_dim = [box_dim.x/2 - center.x - 2*side_thickness, box_dim.y-2*side_thickness];
    echo(side_dim);
    if ( side_dim.x > 5 ) {
        for (xx = [-1, 1]) {
            translate([xx * (box_dim.x/2 + center.x)/2, 0, side_thickness]) {
                linear_extrude(top_h) {
                    rounded_square(side_dim, corner_r = round_r - side_thickness, center=true);
                }
            }
        }
    }
}
