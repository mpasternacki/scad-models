include <MCAD/stepper.scad>

HELPERS = false;

thickness = 1.2;
corner_r = 3;

screwhead_h = 2.5;
screwhead_d = 6.5;

nema_side = 43;

t8_pos = [21-50.8, -(53.5-17.15+0.5), 0];

module at_nema_screws() {
    for (x=[-0.5, 0.5], y=[-0.5, 0.5]) {
        translate([x*31, y*31]) {
            children();
        }
    }
}

module nema_cutout() {
    difference() {
        square(nema_side, center=true);
        circle(d=25, $fn=96);
        at_nema_screws() {
            circle(d=3.5, $fn=32);
        }
        children();
    }
}

render() difference() {
    union() {
        /* motor mount */
        rotate([90, 0, 0]) translate([0, -nema_side/2, -thickness])
            linear_extrude(height=thickness) {
            nema_cutout() {
                translate([-3, -50]) {
                    square([6, 50]);
                }
                translate([-3-corner_r, -nema_side/2]) {
                    difference() {
                        square(corner_r);
                        translate([0, corner_r]) circle(r=corner_r, $fn=24);
                    }
                }
                translate([3, -nema_side/2]) {
                    difference() {
                        square(corner_r);
                        translate([corner_r, corner_r]) circle(r=corner_r, $fn=24);
                    }
                }
            }
        }

        /* plate mount */
        /* TODO: nuts at bottom instead of screwheads? Or heat set threading inserts? */
        render() translate([0, -nema_side/2, 0]) {
            difference() {
                linear_extrude(height=thickness+screwhead_h) {
                    nema_cutout();
                }
                at_nema_screws() {
                    translate([0,0,-0.1]) {
                        cylinder(d=screwhead_d, h=screwhead_h+0.1, $fn=32);
                    }
                }
            }
        }

        /* sides */
        side_margin = 8;
        for (x=[-nema_side/2, nema_side/2]) {
            hull() {
                translate([x, -nema_side+corner_r, thickness+screwhead_h-side_margin+corner_r])
                    rotate([0, 90, 0]) cylinder(r=corner_r, h=thickness, center=true, $fn=24);
                translate([x, (thickness-nema_side)/2, thickness+screwhead_h-side_margin/2+corner_r/2]) {
                    cube([thickness, thickness+nema_side, side_margin-corner_r], center=true);
                }
                translate([x, -side_margin+corner_r+thickness, corner_r-nema_side])
                    rotate([0, 90, 0]) cylinder(r=corner_r, h=thickness, center=true, $fn=24);
                translate([x, thickness-side_margin/2+corner_r/2, (thickness+screwhead_h-nema_side)/2]) {
                    cube([thickness, side_margin-corner_r, nema_side+thickness+screwhead_h], center=true);
                }
            }
        }

        /* front (with overlap) */
        translate([-(nema_side+thickness)/2, 0, 0]) {
            dim = [nema_side+thickness, thickness, thickness+screwhead_h];
            cube(dim);
            translate([0, -0.1, 0]) cube(dim);
            translate([0, 0, -0.1]) cube(dim);
        }

        /* join for the T8 nut cutout */
        translate([-(nema_side+thickness)/2, -nema_side, -1.5]) {
            cube([4, 15, 2.1]);
        }

    }

    translate([0, -nema_side/2, -2]) {
        at_nema_screws() {
            cylinder(d=screwhead_d, h=screwhead_h+2);
        }
    }

    translate([-25, thickness-corner_r, thickness+screwhead_h-corner_r])
        rotate([90, 0, 0])
        rotate([0, 90, 0])
        render() difference() {
        cube([corner_r, corner_r, 50]);
        cylinder(r=corner_r, h=50, $fn=24);
    }

    /* space for T8 nut */
    translate(t8_pos + [0, 0, -4.5+thickness+screwhead_h]) {
        cylinder(d=23, h=30, $fn=64);
    }

}

color("orange") {
/* Top part (cable duct & chain ends) */
/* large chain; X to be preserved; [-29.85, 7.8, -26.25] with [-90, 0, 180] rotation overlaps the original duct */
    /* y=1.645 offset aligns back of chain with back of small chain / part of duct above extruder */
    translate([-29.85, 1.645, -60]) rotate([-135, 0, 180]) difference() {
        translate([0, 5.8, 6]) import("vendor/cableChain_link_v3.stl", convexity=10);
        translate([-20, -20-2, -1]) cube([40, 20, 23]);
    }

/* small chain; Z to be preserved; [15, 7.5, 2.5] with no rotation overlaps the original duct */
    translate([27.5, 7.5, 2.5]) rotate([90, -90, 0]) union() {
        difference() {
            translate([-1.5, 7.75, 0]) import("vendor/slim-link.stl", convexity=10);
            translate([-10, 0, -1]) cube(20);
        }

        /* TODO: prettier rounded corners than this */
        /* duct above extruder */
        linear_extrude(height=1.65) {
            offset(r=-2, $fn=16) offset(r=2, $fn=16) polygon(
                points=[[-9.5, -0.1],
                        [-9.5, 2],
                        [-1.5, 7.5],
                        [-1.5, 45],
                        [-7.5, 51],
                        // right bottom
                        [-7.5, 69.5],
                        [9.5, 69.5],
                        [9.5, 0],
                        [9.5, -0.1]]);
        };

        /* upwards from the big chain end */
        translate([0, 0, 7])
        linear_extrude(height=1.375) {
            offset(r=-2, $fn=16) offset(r=2, $fn=16) polygon(
                points=[[-47, 45.35],
                        [-47, 51],
                        [-20, 51],
                        [-20, 69.35],
                        [-62.3, 69.35],
                        // bottom big chain
                        [-62.3, 45.35]]);
        }
    }
}

if (HELPERS) {
    color("cyan", 0.67) motor(Nema17, pos=[0, 1.9, -nema_side/2], orientation=[90, 0, 0]/*[-90, -90, 0]*/);

    color("cyan", 0.8)
        translate([21, 34, -0.5])
        rotate([90, -90, 0])
        import("vendor/BMG_block.stl");

    color("blue") translate([0, -nema_side/2-0.5, thickness+screwhead_h])
        /* https://github.com/Creality3DPrinting/Ender-3/blob/master/Ender-3%20Mechanical/PDF/E%20motor%20bracket.pdf */
        linear_extrude(height=2.5) translate([21, -32]) mirror([1, 0, 0]) {
        difference() {
            hull() {
                translate([5, 5]) circle(r=5, $fn=24);
                translate([5, 53.5-5]) circle(r=5, $fn=24);
                translate([41, 53.5-5]) circle(r=5, $fn=24);
                translate([64.9, 2.5]) circle(r=2.5);
            }
            for(x=[5.5, 36.5], y=[17.15, 48]) {
                translate([x, y]) {
                    circle(d=3.5, $fn=24);
                }
            }
            translate([21, 32.5]) {
                circle(d=23, $fn=48);
            }
            hull() {
                for (dy=[1.5/2, -1.5/2]) {
                    translate([50.8, 17.15+dy]) {
                        circle(d=10.3, $fn=32);
                    }
                }
            }
            for (y=[8.15, 26.15]) {
                hull() {
                    for (dy=[1.7/2, -1.7/2]) {
                        translate([50.8, y+dy]) {
                            circle(d=3, $fn=16);
                        }
                    }
                }
            }
        }
    }

    color("blue") translate(t8_pos) {
        cylinder(d=8, h=100, center=true, $fn=24);
    }

    color("cyan") translate(t8_pos + [0, 0, thickness+screwhead_h]) {
        translate([0, 0, -15]) difference() {
            union() {
                translate([0, 0, 11]) cylinder(d=22, h=4, $fn=24);
                cylinder(d=10.5, h=15, $fn=24);
            }
            cylinder(d=8.2, h=32, center=true, $fn=16);
            for (pos=[[0, 8], [0, -8], [8, 0], [-8, 0]]) {
                translate(pos)
                    cylinder(d=3, h=32, center=true, $fn=12);
            }
        }
    }

    * color("pink", 0.5) translate([-119.8, 7.7, -118]) rotate([90, 0, 0]) import("vendor/extruder-mount-27mm-alu-dd.stl");
}
