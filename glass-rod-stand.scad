use <exponential-column.scad>;

rod_d = 7;
rod_n = 5;
rod_circle_r = (rod_d * rod_n * 1.4) / (2 * 3.14159);
thickness = 2;
height = 120;
bottom_d = 65; // (rod_circle_r*2 + rod_d) * 2.5;


angle = 360 / rod_n;

difference() {
    maybe_exponential_column(d1=bottom_d, d2=2*rod_circle_r, h=height, $fn=48, $layer=1);

    for (i = [0:rod_n-1]) {
        translate([rod_circle_r * sin(angle*i), rod_circle_r*cos(angle*i), thickness]) {
            cylinder(d=rod_d, h=height, $fn=24);
        }
    }
}

