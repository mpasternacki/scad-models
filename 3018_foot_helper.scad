xx = [0, 5];
yy = [0, 20];
linear_extrude(height=2) {
    difference() {
        hull() {
            for(x=xx, y=yy) translate([x, y]) circle(d=10, $fn=32);
        }
        for(y=yy) translate([0, y]) circle(d=4.2, $fn=24);
    }
}
