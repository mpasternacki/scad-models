use <./dotSCAD/src/rounded_cube.scad>;
use <./dotSCAD/src/rounded_square.scad>;

width = 12.5;
shaft_d = 7;
thickness = 2;
length = 32;
height = 23;
corner_r = 5;

cable_hole_dim = [6, 2];
precision = 0.2;

$fn = 24;
epsilon = 0.08;

difference() {
    translate([-thickness, -thickness, -thickness]) {
        rounded_cube([length+2*thickness, width+2*thickness, height+thickness+corner_r], corner_r=corner_r);
    }
    translate([-thickness-epsilon, -thickness-epsilon, height]) {
        cube([length+2*(thickness+epsilon), width+2*(thickness+epsilon), corner_r+epsilon]);
    }
    translate([length/2, width/2, -thickness-epsilon]) {
        cylinder(d=shaft_d, h=thickness+2*epsilon);
    }
    linear_extrude(height+epsilon) {
        rounded_square([length, width], corner_r=corner_r-thickness);
    }

    translate([-thickness-epsilon, width/2 - cable_hole_dim.x/2, height - (thickness + cable_hole_dim.y)]) {
        cube([thickness+2*epsilon, cable_hole_dim.x, thickness + cable_hole_dim.y + epsilon]);
    }
}


translate([0, width*2, 0]) {
    difference() {
        translate([-thickness-precision, -thickness-precision, -corner_r]) {
            rounded_cube([length+2*thickness, width+2*thickness, 2*corner_r], corner_r=corner_r);
        }
        translate([-thickness-precision, -thickness-precision, 0]) {
            cube([length+2*thickness, width+2*thickness, corner_r+epsilon]);
        }
    }
    linear_extrude(thickness) {
        rounded_square([length-2*precision, width-2*precision], corner_r=corner_r-thickness-precision);
    }
    translate([-thickness-precision, (width-2*precision)/2 - (cable_hole_dim.x-precision)/2, -epsilon]) {
        cube([thickness+precision+epsilon, cable_hole_dim.x-precision, thickness+epsilon]);
    }
}
