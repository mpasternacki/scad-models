use <./dotSCAD/src/rounded_square.scad>;

mounts = [
    [9, 4],
    [124, 4],
    [9, 78],
    [127, 75.574],
    ];

batt_dim = [85, 42, 6.2];
batt_pos = mounts[0] + [7, 7];
batt_cable_w = 3;

// size = [130, 82];          /* without antenna */
size = [130, 92];          /* with antenna */

sma_w = 10.5;
sma_z = 2.4;
sma_l = 5.5;
antenna_socket_y = 82;
clock_socket_y = 10;

thickness = 2;
corner_r = 5;

difference() {
    translate([-thickness, -thickness]) {
        linear_extrude(batt_dim.z + thickness) {
            rounded_square([size.x+2*thickness, size.y+2*thickness], corner_r=corner_r, $fn=24);
        }
    }

    /* battery */
    translate([batt_pos.x, batt_pos.y, thickness + 0.01]) {
        cube(batt_dim);
    }

    /* battery cable */
    translate([batt_pos.x - batt_cable_w/2, -thickness-0.1, thickness + 0.01]) {
        cube([batt_cable_w, batt_pos.y + thickness + 0.1 + batt_dim.y/4, batt_dim.z]);
    }

    /* antenna socket */
    translate([size.x - sma_w, antenna_socket_y - sma_l, batt_dim.z + thickness - sma_z]) {
        cube([sma_w + thickness + 0.1, size.y - antenna_socket_y + sma_l + thickness + 0.1, sma_z + 0.1]);
    }

    /* clock socket */
    translate([size.x - sma_l, clock_socket_y - 0.5, batt_dim.z + thickness - sma_z]) {
        cube([thickness + sma_l + 0.1, sma_w + 1, sma_z + 0.1]);
    }

    /* USB sockets */
    translate([batt_pos.x, -thickness-0.1, batt_dim.z + thickness - 3]) {
        cube([32, thickness + 2.4 + 0.1, 3.1]);
    }

    /* holes for filament pieces */
    for (pos = mounts) {
        translate([pos.x, pos.y, thickness]) {
            cylinder(d=2.1, h=batt_dim.z+thickness, $fn=12);
        }
    }
}
/* for (pos = mounts) { */
/*     translate(pos) { */
/*         cylinder(d=2.8, h=batt_dim.z+thickness+3, $fn=12); */
/*     } */
/* } */
