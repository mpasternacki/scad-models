// phone = [72, 142, 6]; // iphone 8
phone = [82, 164, 11]; // iphone 13 pro max
thickness = 2.5;
corner_r=1.2;
angle = 20;

camera = [45, 45, 2.6];

livboj_dia_margin = 0.33;
livboj_dia_bottom = 91 + livboj_dia_margin;
livboj_dia_top = 89.953 + livboj_dia_margin;
livboj_height = 10;
// livboj_center_y = 70; // iphone 8
livboj_center_y = 82; // iphone 13 pro max

voronoi_grid = 23; // 10.5;
voronoi_thickness = 4; // 1.5;

use <dotSCAD/src/rounded_cube.scad>;
use <dotSCAD/src/rounded_square.scad>;
use <dotSCAD/src/voronoi/vrn2_space.scad>;
use <parts/notch.scad>;

$fn=32;

rotate([angle, 0, 0])
difference() {
    height=thickness + livboj_height + phone.z + camera.z;

    rounded_cube([phone.x+2*thickness, phone.y+2*thickness, 2*height], corner_r=corner_r+thickness, center=true);

    cut = [phone.x+4*thickness, phone.y+4*thickness, 2*height];
    translate([-cut.x/2, -cut.y/2, 0]) {
        cube(cut);
    }

    translate([0, 0, -phone.z]) {
        linear_extrude(height) {
            rounded_square([phone.x, phone.y], corner_r=corner_r, center=true);
        }
    }

    livboj_dy = livboj_center_y-phone.y/2;
    translate([0, livboj_dy, -phone.z-livboj_height]) {
        cylinder(h=livboj_height+phone.z+1, d1=livboj_dia_bottom, d2=livboj_dia_top, $fn=64);
    }

    if(false) translate([0, 0, -height-1]) {
        difference() {
            linear_extrude(height+1) {
                intersection() {
                    difference() {
                        rounded_square([phone.x, phone.y], corner_r=corner_r, center=true);
                        translate([0, livboj_dy]) {
                            difference() {
                                circle(d=livboj_dia_bottom+2*thickness);
                                circle(d=livboj_dia_bottom-2*thickness);
                            }
                        }
                    }

                    voronoi_size = [phone.x+20, phone.y+20];
                    translate(-voronoi_size/2) {
                        vrn2_space(voronoi_size, voronoi_grid, spacing=voronoi_thickness, region_type="circle");
                    }
                }
            }

            linear_extrude(1.6) {
                difference() {
                    delta=thickness;
                    square([phone.x+2*thickness, phone.y+2*thickness], center=true);
                    rounded_square([phone.x-delta, phone.y-delta], corner_r=corner_r, center=true);
                }
            }
        }
    }

    /* camera */
    translate([phone.x/2-camera.x-corner_r, phone.y/2-camera.y-corner_r, -camera.z-phone.z]) {
        rounded_cube(camera + [corner_r, corner_r, corner_r], corner_r=corner_r);
    }

    notch_z = phone.z-corner_r;
    notch_th = thickness*3;
    translate([0, 0, -notch_z]) {
        /* charging port + speakers */
        translate([0, -phone.y/2, 0]) {
            notch_xz([55, notch_th, notch_z], corner_r=corner_r, margin=1);
        }

        /* right button */
        translate([phone.x/2, -phone.y/2 + 110.5, 0]) {
            notch_yz([notch_th, 23, notch_z], corner_r=corner_r, margin=1);
        }

        /* left buttons */
        left_notch_dy = 5;
        translate([-phone.x/2, -phone.y/2 + 116.5 - left_notch_dy/2, 0]) {
            notch_yz([notch_th, 40 + left_notch_dy, notch_z], corner_r=corner_r, margin=1);
        }
    }
}
