module outline2d(d1=5.5, d2=8.5) {
    difference() {
        hull() {
            for(x=[-10, 10]) {
                translate([x, 0]) {
                    circle(r=15, $fn=32);
                }
            }

            translate([0, 30]) {
                circle(r=15, $fn=32);
            }
        }

        for(x=[-10, 10]) {
            translate([x, 0]) {
                circle(d=d1, $fn=32);
            }
        }

        translate([0, 30]) {
            circle(d=d2, $fn=32);
        }
    }
}

bottom_thickness = 1;
bearing_h = 5;
bearing_d_outer = 16.1;
bearing_d_inner = 8.2;
screwhead_d = 8;
screwhead_h = 2.5;
screw_clearance_d = 5.5;

total_h = bottom_thickness + bearing_h;
screw_clearance_h = total_h - screwhead_h;

difference() {
    union() {
        linear_extrude(height=total_h) {
            outline2d(d1=screwhead_d, d2=bearing_d_outer);
        }
        linear_extrude(height=screw_clearance_h) {
            outline2d(d1=screw_clearance_d, d2=bearing_d_outer);
        }
        linear_extrude(height=bottom_thickness) {
            outline2d(d1=screw_clearance_d, d2=bearing_d_inner);
        }
    }

    translate([0, 0, -1]) {
        linear_extrude(height=total_h+2) {
            offset(r=1) offset(r=-1)
                polygon([[20, 22.5],
                         [5, 37.5],
                         [5, 50],
                         [50, 50],
                         [50, 22]]);
        }

        translate([3, 42, 0]) cube([10, 10, 50]);
    }

}

// % cube([40, 20, 50], center=true);
