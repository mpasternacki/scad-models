use <dotSCAD/src/rounded_cube.scad>;
use <dotSCAD/src/rounded_square.scad>;

station_dim = [132, 90, 41];
back_margin = 5;
back_margin_r = 5;
bottom_margin = 15;
bottom_margin_r = 15;
thickness = 2;
top_thickness = 5;
angle = 10;

screwhole_d = 3.6;
screwhead_d = 8.2;
screwhole_l = thickness;

screwhead_l = (screwhead_d - screwhole_d) / (2 * sqrt(2));
assert (top_thickness >= screwhole_l + screwhead_l);
projection_dim = [station_dim.x, station_dim.y*cos(angle)];
screwhole_offset = bottom_margin/2;
screwhole_dy = sin(angle) * (station_dim.z + top_thickness);
screwhole_pos = [
    [screwhole_offset, screwhole_offset + screwhole_dy],
    [projection_dim.x-screwhole_offset, screwhole_offset + screwhole_dy],
    [projection_dim.x-screwhole_offset, projection_dim.y-screwhole_offset],
    [screwhole_offset, projection_dim.y-screwhole_offset],
    ];


epsilon = $preview ? 0.01 : 0;

module station(extension=10, dz=0) {
    cube(station_dim /* + [0, 0, dz] */);
    translate([0, 0, -extension]) {
        linear_extrude(extension+epsilon) {
            intersection() {
                translate([bottom_margin, -bottom_margin_r-bottom_margin]) {
                    rounded_square([station_dim.x-2*bottom_margin, station_dim.y+bottom_margin_r], corner_r=bottom_margin_r, $fn=24);
                }
                square([station_dim.x, station_dim.y]);
            }
        }
    }
    translate([bottom_margin, 0, back_margin]) {
        cube([station_dim.x-2*bottom_margin, station_dim.y+extension, station_dim.z+dz-back_margin]);
    }
    translate([0, station_dim.y, 0]) {
        rotate([90, 0, 0]) {
            translate([0, 0, -extension]) {
                linear_extrude(extension+.1) {
                    intersection() {
                        translate([back_margin, back_margin]) {
                            rounded_square([station_dim.x-2*back_margin, station_dim.z-2*back_margin/* +back_margin_r */], corner_r=back_margin_r);
                        }
                        square([station_dim.x, station_dim.z]);
                    }
                }
            }
        }
    }
}

dz = tan(angle) * (station_dim.y + thickness) + top_thickness / cos(angle);
echo(dz);
top_z = station_dim.z * cos(angle) + top_thickness;

module bottom() {
    intersection() {
        rotate([-angle, 0, 0]) {
            difference() {
                translate([-thickness, epsilon, -thickness]) {
                    intersection() {
                        rounded_cube([station_dim.x+2*thickness, station_dim.y+thickness, station_dim.z+2*thickness+dz], corner_r=thickness, $fn=16);
                        cube([station_dim.x+2*thickness, station_dim.y+2*thickness, station_dim.z+thickness+dz]);
                    }
                }
                station(extension=thickness+1, dz=dz+epsilon);
            }
        }
        cube([4*station_dim.x, 4*station_dim.y, 2*top_z], center=true);
    }
}

module screwhole() {
    sh_dz = dz+thickness;
    sh_l = sh_dz + top_z + epsilon;
    translate([0, 0, -sh_dz]) {
        cylinder(d=screwhole_d, h=sh_l);
        cylinder(d=screwhead_d, h=sh_l - (screwhole_l + screwhead_l));
    }
    translate([0, 0, top_z - (screwhole_l + screwhead_l + epsilon)]) {
        cylinder(d1=screwhead_d, d2=0, h=screwhead_d/2);
    }
}

difference () {
    bottom();
    for (pos = screwhole_pos) {
        /* TODO */
        translate(pos) {
            screwhole($fn=16);
        }
    }
}
