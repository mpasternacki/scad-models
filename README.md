SCAD Models
===========

My personal repository of [OpenSCAD](https://www.openscad.org/)
models. It's not intended to be useful for anybody, but there's no
reason to keep it closed and I might want to link to it.

Some of the models need
[https://github.com/JustinSDK/dotSCAD](JustinSDK/dotSCAD) libraries in
`OPENSCADPATH` ([documentation with a good intro to OpenSCAD
itself](https://openhome.cc/eGossip/OpenSCAD/)).

