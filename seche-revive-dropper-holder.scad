d1_bottom = 19.2;
d1_top = 18.2;
d2 = 8;

h = 30;
h1 = 15;
h1_top = 35;
h2 = 46;
thickness = 2.6;
dist = 0*thickness; // 8;
offset_r = 2;

$fn = 64;
x2 = (d1_bottom+d2)/2;

function tanh(x) = (exp(2*x)-1) / (exp(2*x)+1);

module transition(x0, y0, x1, y1, steps=100, stretch=1) {
    assert( x1 > x0);
    step = (x1 - x0) / steps;
    y_scale = (y1 - y0);
    dy = min(y0, y1);
    polygon(points=[
                [x0, 0],
                for (dx = [0:steps]) [x0 + dx*step, (tanh(stretch*(x0+dx*step))/2+0.5)*y_scale + y0],
                [x1, 0],
                ]);
}

d_max = max(d1_bottom, d1_top, d2);
total_width = 2*d_max + dist + 2*thickness;
total_depth = d_max + 4 * thickness;
total_h = max(h1, h2);

intersection() {
    difference() {
        linear_extrude(total_h) {
            offset(r=-offset_r) {
                offset(r=offset_r) {
                    circle(d=d1_bottom+thickness*2);
                    translate([x2, 0]) {
                        circle(d=d2+thickness*2);
                    }
                }
            }
        }
        translate([0, 0, -0.01]) {
            cylinder(d1=d1_bottom, d2=d1_top, h=h1_top);
        }
        translate([x2, 0, d2/2-0.1*d2]) {
            cylinder(d=d2, h=total_h+0.02);
            sphere(d=d2);
        }
    }

    translate([x2/2, total_depth/2, 0]) rotate([90, 0, 0]) {
        linear_extrude(total_depth) {
            transition(-total_width/2, h1, total_width/2, h2, stretch=0.5);
        }
    }
}
