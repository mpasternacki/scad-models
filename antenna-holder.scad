module negative(dh=2, dw=0) {
    translate([0, 10, dh]) cylinder(d=6.5, h=35-dh, $fn=24);

    hull() {
        translate([-9, -5.5]) cube([18, 11, 18]);
        cylinder(d=10, h=30, $fn=32);
    }

    hull() {
        cylinder(d=5.5, h=35,  $fn=24);
        translate([0, -dw, 0]) cylinder(d=5.5, h=35,  $fn=24);
    }
}

difference() {
    intersection() {
        minkowski() {
            hull() negative(dh=0);
            cylinder(r=2, h=0.0001);
        }
        translate([-100, -100, 0]) cube([200, 200, 35]);
    }
    negative(dw=10);
}
